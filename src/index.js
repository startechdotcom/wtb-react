import React from 'react';
import ReactDom from 'react-dom';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// https://fontawesome.com/how-to-use/font-awesome-api
// https://fontawesome.com/icons?d=gallery
// https://fontawesome.com/how-to-use/on-the-web/using-with/react
import { 
    faBell, 
    faCog,
    faQuestion,
    faUser,
    faEye,
    faEyeSlash,
    faSearch,
    faEllipsisH,
    faQuestionCircle,
    faFilter,
    faCheck,
    faTimes,
    faToggleOn,
    faToggleOff,
    faArrowLeft,
    faArrowRight } from '@fortawesome/free-solid-svg-icons'

library.add(
    faBell, 
    faCog,
    faQuestion,
    faUser,
    faEye,
    faEyeSlash,
    faSearch,
    faEllipsisH,
    faQuestionCircle,
    faFilter,
    faCheck,
    faTimes,
    faToggleOn,
    faToggleOff,    
    faArrowLeft,
    faArrowRight
);

import App from './app';


const rootApp = document.getElementById('root');
      ReactDom.render(<App/>, rootApp);