import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ImageLoader from 'react-load-image';


// https://react-bootstrap-table.github.io/react-bootstrap-table2/
// http://allenfang.github.io/react-bootstrap-table/example.html
// https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/table-props.html
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
//import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import filterFactory, { textFilter, selectFilter } from 'react-bootstrap-table2-filter';
import overlayFactory from 'react-bootstrap-table2-overlay';

import { 
    Col,
    Row,
    Container,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button
} from 'reactstrap'; 

import Partner from './partner-detail';


const { SearchBar } = Search,
      FILTER_CLOSE = "body--filter-close";
const columns = [],
      countryOptions = [{
            value: '',
            label: ''
        }, {
            value: 'AU',
            label: 'Australia'
        }, {
            value: 'AT',
            label: 'Austria'
        }, {
            value: 'BE',
            label: 'Belgium'
        },  {
            value: 'CA',
            label: 'Canada'
        }, {
            value: 'CL',
            label: 'Chile'
        }, {
            value: 'CZ',
            label: 'Czech Republic'
        }, {
            value: 'DK',
            label: 'Denmark'
        }, {
            value: 'EU',
            label: 'Europe'
        }, {
            value: 'FI',
            label: 'Finland'
        }, {
            value: 'FR',
            label: 'France'
        }, {
            value: 'DE',
            label: 'Germany'
        }, {
            value: 'HU',
            label: 'Hungary'
        }, {
            value: 'INTRL',
            label: 'International'    
        }, {
            value: 'IT',
            label: 'Italy'
        }, {
            value: 'JP',
            label: 'Japan'
        }, {
            value: 'MX',
            label: 'Mexico'
        }, {
            value: 'NL',
            label: 'Netherlands'
        }, {
            value: 'NZ',
            label: 'New Zealand'
        }, {
            value: 'PT',
            label: 'Portugal'
        }, {
            value: 'IE',
            label: 'Republic of Ireland'
        }, {
            value: 'ES',
            label: 'Spain'
        }, {
            value: 'SE',
            label: 'Sweden'
        }, {
            value: 'CH',
            label: 'Switzerland'
        }, {
            value: 'UK',
            label: 'United Kingdom'
        },  {
            value: 'US',
            label: 'United States'
        }];

let options = [],
    rowEvents,
    companyFilter,
    websiteFilter;

var iframeURL = "",
    headerCountry = {
        'AU': 'Australia',
        'AT': 'Austria',
        'BE': 'Belgium',
        'CA': 'Canada',
        'CL': 'Chile',
        'CZ': 'Czech Republic',
        'DK': 'Denmark',
        'EU': 'Europe',
        'FI': 'Finland',
        'FR': 'France',
        'DE': 'Germany',
        'HU': 'Hungary',
        'INTRL': 'International', 
        'IT': 'Italy',
        'JP': 'Japan',
        'MX': 'Mexico',
        'NL': 'Netherlands',
        'NZ': 'New Zealand',
        'PT': 'Portugal',
        'IE': 'Republic of Ireland',
        'ES': 'Spain',
        'SE': 'Sweden',
        'CH': 'Switzerland',
        'UK': 'United Kingdom',
        'US': 'United States'    
    };

class WTBtable extends React.Component { 
    constructor(props){
        super(props);
        this.updateIsPartner = this.updateIsPartner.bind(this);

        this.state = {
            table: [], 
            rowClasses: "rows",
            rowSelected: -1,
            selRow: [],
            openPartner: false,
            complete: false,
            closePartner: false,
            createPartner: false,
            toggleFilter: true, 
            columnOption: []
        };
    }
    componentDidMount() {
        document.body.classList.add(FILTER_CLOSE);
        this.updateIsPartner(false);
        this.init();
    } 
    componentWillUnmount() {
        document.body.classList.remove(FILTER_CLOSE);
      }
    updateIsPartner = (value) => {
            if (value == false) {
                if (this.state.rowSelected >= 0) {
                    var myRowElements = document.getElementById('wtb-table').getElementsByTagName('tr');
                    myRowElements[this.state.rowSelected+1].classList.remove('row-selected');
                }
            }
        this.setState({ openPartner: value, createPartner: value });
    }
    addPartner = () => {
        this.setState({ createPartner: true });
    }
    toggleFilterAll = () => {
        (!this.state.toggleFilter) ? 
            document.body.classList.add(FILTER_CLOSE) 
        : 
            document.body.classList.remove(FILTER_CLOSE);
            companyFilter('');
            websiteFilter('');    
        this.setState({ toggleFilter: !this.state.toggleFilter });
    }
    CountryFormatter = (cell) => {
        let found = [],
              theLabel = "";
        if (typeof cell !== 'undefined') {
            found = countryOptions.find(x => { 
                if (x.value == cell) { 
                    return x; 
                } 
            }); 
            
            theLabel = (typeof found !== 'undefined') ? <span><span className={`flag flag-${found.value.toLowerCase()}`}></span> {found.label}</span> : "" ;
        }else {
            theLabel = "";
        }
        return (
            <div>{theLabel}</div>
        );        
    }
    Checks = (cell) => {
        if (cell == "TRUE") {
            return ( <span><FontAwesomeIcon icon="check" /></span> ); 
        }
    }
    generateColumnOption = (values) => {
        for (var keyResult in values) {
            switch (keyResult) {
                case 'WebPartnerId':
                    columns.push({
                        dataField: keyResult,
                        text: "ID",
                        hidden: true,
                        classes: (cell, row, rowIndex, colIndex) => {
                            if (row.Active === "TRUE") return 'cell-active';
                            return 'cell-disable';
                        }
                    });
                    break;
                case 'PartnerCode':
                    columns.push({
                        dataField: keyResult,
                        text: "Code",
                        sort: true,
                        onSort: (field, order) => {
                            console.log(field)
                        },
                        className: (this.state.toggleFilter) ? 'hide-filter' : "",
                        // filter: textFilter({
                        //     className: 'filters'
                        // }),
                        classes: (cell, row, rowIndex, colIndex) => {
                            if (row.Active === "TRUE") return 'cell-active column-PartnerCode';
                            return 'cell-disable column-PartnerCode';
                        }
                    });
                    break;
                case 'PartnerName':
                    columns.push({
                        dataField: keyResult,
                        text: "Company",
                        sort: true,
                        onSort: (field, order) => {
                            console.log(field)
                        },
                        headerSortingClasses,
                        filter: textFilter({
                            className: 'filters',
                            getFilter: (filter) => {
                                companyFilter = filter;
                            }
                        }),
                        classes: (cell, row, rowIndex, colIndex) => {
                            if (row.Active === "TRUE") return 'cell-active column-PartnerName';
                            return 'cell-disable column-PartnerName';
                        }
                    });
                    break;     
                case 'PartnerType':
                    columns.push({
                        dataField: keyResult,
                        text: "Type",
                        sort: true,
                        onSort: (field, order) => {
                            console.log(field)
                        },
                        headerSortingClasses,
                        classes: (cell, row, rowIndex, colIndex) => {
                            if (row.Active === "TRUE") return 'cell-active column-PartnerType';
                            return 'cell-disable column-PartnerType';
                        }
                    });
                    break;                      
                case 'LinkedFromHostname':
                    columns.push({
                        dataField: keyResult,
                        text: "Website",
                        sort: true,
                        headerSortingClasses,
                        formatter: this.CountryFormatter,
                        filter: selectFilter({
                            options: headerCountry,
                            className: 'filters',
                            getFilter: (filter) => {
                                websiteFilter = filter;
                            }
                        }),
                        classes: (cell, row, rowIndex, colIndex) => {
                            if (row.Active === "TRUE") return 'cell-active column-LinkedFromHostname';
                            return 'cell-disable column-LinkedFromHostname';
                        }
                        //sort: true,
                        // editor: {
                        //     type: Type.SELECT,
                        //     options: countryOptions
                        // }
                    });
                    break;    
                case 'Active':
                    columns.push({
                        dataField: keyResult,
                        text: keyResult,
                        formatter: this.Checks,
                        headerAlign: "center",
                        align: 'center',
                        classes: (cell, row, rowIndex, colIndex) => {
                            if (row.Active === "TRUE") return 'cell-active column-active';
                            return 'cell-disable column-active';
                        }
                    });
                    break;                                             
                case 'WTBPartner':
                    columns.push({
                        dataField: keyResult,
                        text: "WTB Partner",
                        formatter: this.Checks,
                        headerAlign: "center",
                        align: 'center',
                        classes: (cell, row, rowIndex, colIndex) => {
                            if (row.Active === "TRUE") return 'cell-active column-wtb';
                            return 'cell-disable column-wtb';
                        },
                        // editor: {
                        //     type: Type.SELECT,
                        //     options: [{
                        //       value: 'TRUE',
                        //       label: 'TRUE'
                        //     }, {
                        //       value: 'FALSE',
                        //       label: 'FALSE'
                        //     }]
                        // }

                    });
                    break;   
                case 'ConXitPartner':
                    columns.push({
                        dataField: keyResult,
                        text: "ConXit",
                        formatter: this.Checks,
                        headerAlign: "center",
                        align: 'center',
                        classes: (cell, row, rowIndex, colIndex) => {
                            if (row.Active === "TRUE") return 'cell-active column-conxit';
                            return 'cell-disable column-conxit';
                        },
                        // editor: {
                        //     type: Type.SELECT,
                        //     options: [{
                        //       value: 'TRUE',
                        //       label: 'TRUE'
                        //     }, {
                        //       value: 'FALSE',
                        //       label: 'FALSE'
                        //     }]
                        // }

                    });
                    break;  
                case 'BorrowSKUsFrom':
                    columns.push({
                        dataField: keyResult,
                        text:  <span>Use Skus from <a onClick={this.toggleFilterAll} className={(this.state.toggleFilter) ? "filter-icon non-activated" : "filter-icon activated"} style={ { cursor: 'pointer' } }><FontAwesomeIcon icon="filter" /></a></span>,
                        classes: (cell, row, rowIndex, colIndex) => {
                            if (row.Active === "TRUE") return 'cell-active column-borrow';
                            return 'cell-disable column-borrow';
                        }
                    });
                    break;
                case 'Audit':
                    columns.push({
                        dataField: keyResult,
                        text: keyResult,
                        formatter: this.Checks,
                        headerAlign: "center",
                        align: 'center',
                        classes: (cell, row, rowIndex, colIndex) => {
                            if (row.Active === "TRUE") return 'cell-active column-audit';
                            return 'cell-disable column-audit';
                        }
                    });
                    break;                                                                                               
                default:     
                    // columns.push({
                    //     dataField: keyResult,
                    //     text: keyResult
                    // });          
            }
        }
        this.setState({ columnOption: columns });        
    }
    init = () => {
        var result = [];
        console.log("WTB table "+this.props.myData);
        for (var objResult in this.props.myData) {
           result.push(this.props.myData[objResult]);
        }         
        this.setState({ table: result, complete: true });
        
        this.generateColumnOption(result[0]);
       
        options = {
            paginationSize: 6,
            pageStartIndex: 1,
            // alwaysShowAllBtns: true, // Always show next and previous button
            withFirstAndLast: false, // Hide the going to First and Last page button
            // hideSizePerPage: true, // Hide the sizePerPage dropdown always
            // hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
            firstPageText: 'First',
            prePageText: 'Previous',
            nextPageText: 'Next',
            lastPageText: 'Last',
            nextPageTitle: 'First page',
            prePageTitle: 'Pre page',
            firstPageTitle: 'Next page',
            lastPageTitle: 'Last page',
            showTotal: true,
            paginationTotalRenderer: customTotal,
            sizePerPageList: [{
                text: '10', value: 10
            }, {
                text: '25', value: 25
            }, {
                text: '50', value: 50
            }, {
                text: '100', value: 100
            }, {
                text: 'All', value: result.length
            }]
        };
 
        rowEvents = {
            onClick: (e, row, rowIndex) => {
                var myRowElements = document.getElementById('wtb-table').getElementsByTagName('tr');
                    myRowElements[this.state.rowSelected+1].classList.remove('row-selected');
                    myRowElements[rowIndex+1].classList.add('row-selected');
               this.setState({ selRow: row, openPartner: true, rowSelected: rowIndex });
            }
        };   
    }
    render() {
        return (
            <Container className="h-100">
                {( this.state.complete ) ? 
                <div className="relative-element">
                    <Row id="header-app" className="justify-content-between">
                        <Col sm="8"><h1>Where to Buy Admin Tool</h1></Col>
                        <Col sm="1"><FontAwesomeIcon icon="question-circle" className="float-right fa-lg" /></Col>

                    </Row>
                    <Row id="main-table-wtb" className={ (this.state.openPartner || this.state.createPartner) ? "hide" : "show"}>
                        <ToolkitProvider
                            keyField='WebPartnerId' 
                            data={ this.state.table } 
                            columns={ this.state.columnOption } 
                            loading={ true }  //only loading is true, react-bootstrap-table will render overlay
                            overlay={ overlayFactory() }
                            version="4"
                            search
                        >
                            {
                                props => (
                                    <Col>
                                        <Col>
                                            <Row>
                                                <Col sm="7"><h2>Partner</h2></Col>
                                                <SearchBar 
                                                    { ...props.searchProps } 
                                                    className="main-wtb-search col-sm-3"
                                                    delay={ 1000 }
                                                />
                                                {/* <MySearch { ...props.searchProps } /> */}
                                                <Col sm="2"><Button onClick={this.addPartner} className="col no-padding" color="success">New Partner</Button></Col>
                                            </Row>
                                        </Col>
                                        <BootstrapTable
                                            { ...props.baseProps }
                                            id="wtb-table"
                                            wrapperClasses="col-sm-12"
                                            keyField='WebPartnerId'
                                            noDataIndication="Table is Empty"
                                            bordered={ false }
                                            hover
                                            pagination={ paginationFactory(options) }
                                            // expandRow={ expandRow }
                                            //selectRow={ selectRowProp }
                                            filter={ filterFactory() }
                                            rowEvents={ rowEvents } 
                                        />
                                    </Col>
                                )
                            }
                        </ToolkitProvider>
                    </Row>
                    <Row id="partner-info" className={ (this.state.openPartner || this.state.createPartner) ? "show" : "hide"}>
                        { (this.state.openPartner || this.state.createPartner) ? 
                            <Partner 
                            updateIsPartner={this.updateIsPartner} 
                            selRow={(this.state.createPartner) ? null : this.state.selRow} theValues={this.state.table} 
                            /> 
                        : 
                            null  
                        }
                    </Row>
                </div>
                :
                    <span></span>
                }
            </Container>
        )
    }
}

const MySearch = (props) => {
    let input;
    const handleClick = () => {
        props.onSearch(input.value);
    };
    return (
        <Container id="main-search">
            <Col xs={{ size: 3, offset: 8 }} className="no-right-padding"><input
                className="form-control"
                ref={ n => input = n }
                type="text"
                onBlur={ handleClick }
            /></Col>
            <Col xs="1" className="no-left-padding">
                <button className="btn btn-primary col-12 no-padding" onClick={ handleClick }><FontAwesomeIcon icon="search" /></button>
            </Col>
        </Container>
    );
};

const customTotal = (from, to, size) => (
    <span className="pagination-total">  { from } to { to+1 } of { size } Results</span>
);

const headerSortingClasses = (column, sortOrder, isLastSorting, colIndex) => (
    (sortOrder === 'asc') ? 'sorting-asc' : 'sorting-desc'
);


export default WTBtable;