import React from "react";
import PropTypes from 'prop-types';
import ImageLoader from 'react-load-image';
import { 
    Row,
    Col,
    Nav,  
    Navbar,
    NavbarToggler,
    NavbarBrand, 
    NavItem, 
    NavLink, 
    Container
} from 'reactstrap';  //https://reactstrap.github.io/components/navbar/
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function Preloader(props) {
    return <img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" style={{maxWidth: '100%'}} alt="" />;
}

class Header extends React.Component { 
    constructor(){
        super();
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false,
            isLoggedIn: true
        };
        Container.propTypes = {
            fluid:  PropTypes.bool
        }
    }
    showIcon = () => {
        this.setState({ isLoggedIn: !this.state.isLoggedIn });
        return console.log('error');
    }
    toggle = () => {
        this.setState({  isOpen: !this.state.isOpen });
        this.props.updateIsOpen(!this.props.isOpen);
    }

    render() {
        // console.log('Toggle Clicked: '+this.state.isOpen);
        return (
            <header className="col">
                <Row className="justify-content-between">
                    <Col sm="3" className="column">
                        <Navbar fluid fixedTop>
                            <div className="d-flex flex-row align-items-center">
                                <div className="p-2"><NavbarToggler onClick={this.toggle} className={(this.props.isOpen) ? "open-menu" : ""} /></div>
                                <div className="p-2 bright"><Logo /></div>
                                <div className="p-2"><PageName /></div>
                            </div>
                        </Navbar>
                    </Col>
                    <Col sm="4" className="column">
                        <Nav className="d-flex flex-row align-items-center profile-information-links">
                            <NavItem className="p-2 ml-auto"><NavLink href="#" className="iconLink"><FontAwesomeIcon icon="bell" /></NavLink></NavItem>
                            <NavItem className="p-2"><NavLink href="#" className="iconLink"><FontAwesomeIcon icon="cog" /></NavLink></NavItem>
                            <NavItem className="p-2"><NavLink href="#" className="iconLink"><FontAwesomeIcon icon="question" /></NavLink></NavItem>
                            <NavItem className="p-2 profile-menu">
                                <NavLink href="#" className="iconLink">
                                    <ImageLoader src="https://outlook.office365.com/owa/service.svc/s/GetPersonaPhoto?email=Maria.DaSilva@startech.com&UA=0&size=HR64x64" onError={this.props.showIcon}>
                                        <img alt="Maria Da Silva profile thumbnail image" className="thumb-profile rounded-circle" />
                                        <FontAwesomeIcon icon="user" size="2x" />
                                        <Preloader />
                                    </ImageLoader>
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Col>
                </Row>
            </header>
        )
    }  

}

const Logo = () => {
    return (
        <NavbarBrand>
            <span><img src="./public/media/startech-logo-small-dark.png" /></span>
        </NavbarBrand>
    )
}

const PageName = () => {
    return (
        <div id="page-title"><h6>Workspace</h6></div>
    )
}

export default Header;