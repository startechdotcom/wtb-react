import React from "react";
import { 
    Nav,
    NavItem, 
    NavLink,
    DropdownMenu, 
    DropdownItem,
    Collapse,
    Modal
} from 'reactstrap'; 


// I need to read this.
// https://medium.com/@ruthmpardee/passing-data-between-react-components-103ad82ebd17

class Sidebar extends React.Component { 
    constructor(props){
        super(props);
        this.toggleModal = this.toggleModal.bind(this);

        this.state = {
            modal: this.props.isOpen
        }
    }

    toggleModal = () => {
        this.setState({  modal: !this.state.modal });
        this.props.updateIsOpen(!this.props.isOpen);
    }
    
    render() {
        // console.log('hellow + '+this.props.isOpen);
        return (
             <Modal id="sidebar-nav" isOpen={this.props.isOpen} toggle={this.toggleModal}>
                <Collapse isOpen="true" navbar>
                    <Nav navbar>
                        <NavItem><NavLink href="#">Link</NavLink></NavItem>
                        <NavItem><NavLink href="#">Link</NavLink></NavItem>
                        <DropdownMenu title="Dropdown" id="basic-nav-dropdown">
                            <DropdownItem>Action</DropdownItem>
                            <DropdownItem>Another action</DropdownItem>
                            <DropdownItem>Something else here</DropdownItem>
                            <DropdownItem divider />
                            <DropdownItem>Separated link</DropdownItem>
                        </DropdownMenu>
                    </Nav>
                </Collapse>
            </Modal>
        )
    }
}

export default Sidebar;
