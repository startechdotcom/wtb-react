import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';

import { 
    Col,
    Row,
    Form,
    FormGroup,
    FormText,
    Label,
    Input
} from 'reactstrap'; 
// https://reactstrap.github.io/components/form/

      // Need to generate this list using this json file....
      // http://country.io/data/
      // http://country.io/names.json
const urlCountryNames = "http://country.io/names.json";

class CountryList extends React.Component { 
    constructor(props){
        super(props);

        this.state = {
            countryData: []
        };
    }    
    componentDidMount() {
        this.serverRequest = 
            axios.get(urlCountryNames)
            .then(response => {
                this.setState({ countryData: response.data });
            })
            .catch( error => { console.log(error) } );
    } 
    optionCountry = (selected = true) => {
        if ( selected ) {
            let options = [];
                options.push(<option value="" selected>Select...</option>);
                // console.log(Object.keys(this.state.countryData).length);
            if ( Object.keys(this.state.countryData).length > 0 ) { 
                // console.log(this.state.countryData); 
                for (var objResult in this.state.countryData) {
                    // console.log(" "+ objResult);
                    if (this.props.selectedCountry == null) {
                        options.push(<option value={objResult}><span><span className={`flag flag-${objResult.toLowerCase()}`}></span> {this.state.countryData[objResult]}</span></option>);         
                        
                    }else {
                        if ( objResult == this.props.selectedCountry ) {
                            options.push(<option value={objResult} selected><span><span className={`flag flag-${objResult.toLowerCase()}`}></span> {this.state.countryData[objResult]}</span></option>);
                        }else {
                            options.push(<option value={objResult}><span><span className={`flag flag-${objResult.toLowerCase()}`}></span> {this.state.countryData[objResult]}</span></option>);
                        } 
                    }
                }
                return options;
            }
        }else {
            let countryName = "";
            if ( Object.keys(this.state.countryData).length > 0 ) { 
                for (var objResult in this.state.countryData) {
                        if ( objResult == this.props.selectedCountry ) {
                            countryName = <span className="d-flex justify-content-center">
                                            <span className="p-2">
                                                <span className={`flag flag-${objResult.toLowerCase()}`}></span>
                                            </span> 
                                            <span className="p-2">{this.state.countryData[objResult]}</span>
                                        </span>;
                        }
                }
                return countryName;
            }            
        }
    }
    render() {
        return (
            <span>
                {(this.props.selectedOption) ?
                    <FormGroup>
                        <Label for={this.props.inputID}>{this.props.label}</Label>
                        <Input type="select" name={this.props.inputID} id={this.props.inputID}>
                            {this.optionCountry()}
                        </Input>
                    </FormGroup>
                :
                    <span id={this.props.inputID}>{this.optionCountry(false)}</span>
                }
            </span>
        )
    }
}

export default CountryList;