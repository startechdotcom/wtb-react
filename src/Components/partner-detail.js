import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ImageLoader from 'react-load-image';
import { 
    Col,
    Row,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Button,
} from 'reactstrap'; 
// https://reactstrap.github.io/components/form/

import PartnerCreateUpdate from './partner-create-update';
import CountryList from './country-list';

let img = "";

class Partner extends React.Component { 
    constructor(props){
        super(props);
        this.iframeToggle = this.iframeToggle.bind(this);
        this.closePartner = this.closePartner.bind(this);

        this.state = {
            iframeModal: false,
            openPartner: true,
            editEnable: false
        };
    }
    componentDidMount() {
        this.setState({ openPartner: this.state.openPartner, editEnable: false });
    } 
    closePartner = () => {
        this.setState({ openPartner: !this.state.openPartner, iframeModal: false, editEnable: false });
        this.props.updateIsPartner(!this.state.openPartner);
        console.log('closePArtner'+this.state.openPartner);
    }
    iframeToggle = () => {
        this.setState({ iframeModal: !this.state.iframeModal });
    }
    showDefault = () => {
        return console.log('Partner logo is not available!');
    }
    editPartner = () => {
        this.setState({ editEnable: !this.state.editEnable, editorMenu: false });
    }
 
    // Animation of two divs
    // https://worldisbeautiful.net/
    // https://stackoverflow.com/questions/16982499/css-flip-transition-between-two-divs/16984368
    render() {
        return (
            <Col>
                <Col id="top-partner-menu">
                    <Row className="justify-content-between">
                        <Col sm="2"><a href="#" onClick={this.closePartner} className="goBack"><FontAwesomeIcon icon="arrow-left" className="fa-lg" /></a></Col>
                        {(this.props.selRow == null) ? 
                            <Col sm="4">
                                <Row className="align-items-center">
                                <Col sm="5" className="text-right"><a onClick={this.editPartner}>Cancel</a> </Col>
                                <Col sm="7"><Button className="col no-padding" color="success">Create Partner</Button></Col>
                                </Row>
                            </Col>
                        :
                            <Col sm="4">
                                <Row className={(this.state.editEnable) ? "align-items-center" : "align-items-center justify-content-end"}>
                                    {(this.state.editEnable) ? 
                                        <Col sm="5" className="text-right"><a onClick={this.editPartner}>Cancel</a></Col>
                                    : 
                                        null
                                    }
                                    <Col sm="7"><Button onClick={(this.state.editEnable) ? null : this.editPartner} className="col no-padding" color={(this.state.editEnable) ? "primary" : "success"}>{(this.state.editEnable) ? "Save Partner" : "Edit Partner"}</Button></Col>
                                </Row>
                            </Col>
                        }
                    </Row>
                </Col>
                {(this.props.selRow == null) ?
                    <PartnerCreateUpdate theValues={this.props.theValues}  />
                :
                    <div>
                    {(this.state.editEnable) ?
                        <PartnerCreateUpdate selRow={this.props.selRow} theValues={this.props.theValues}  />
                    :
                        <Row>
                            <Col sm={4} id="left-column-profile">
                                <Col>
                                    <div className="partner-img text-center">
                                    <span className="hide">{ img = this.props.selRow.PartnerCode}</span> 
                                        <ImageLoader key={img} src={`https://sgcdn.startech.com/005329/media/pages/wheretobuy/large/${img.toLowerCase()}.large.gif`} onError={this.props.showDefault}>
                                            <img />
                                            <div><img src="https://sgcdn.preprod.startech.com/005329/media/pages/wheretobuy/large/nan.gif" alt="No preview partner image"  /></div>
                                            <div></div>
                                        </ImageLoader>
                                        {/* <img src={`https://sgcdn.startech.com/005329/media/pages/wheretobuy/large/${img.toLowerCase()}.large.gif`} /> */}
                                    </div>
                                    <h4 className="text-center">{this.props.selRow.PartnerName}</h4>
                                    <h6 className="text-center">{img}</h6> 

                                    <Row className="justify-content-md-center"><Col sm="8"><Button className="col" color="secondary">View SKU List</Button></Col></Row>
                                    <Col>
                                    <Row className="item-group">
                                        <Col sm={3}>
                                            <h6 className="grey-color">Status</h6>  
                                            {(this.props.selRow.Active === "TRUE") ? <span className="active">Active</span> : <span>Disabled</span> }
                                        </Col>
                                        <Col sm={3} className="col-left">
                                            <h6 className="grey-color">Audit</h6>
                                            {(this.props.selRow.Audit === "TRUE") ? <span>Yes</span> : <span>No</span> }             
                                        </Col>  
                                        <Col sm={6} className="col-left">    
                                            <p>{(this.props.selRow.ShowWTBLink === "TRUE") ? <FontAwesomeIcon icon="check" className="active" /> : <FontAwesomeIcon icon="times" className="disabled" /> } WTB Link </p>     
                                            <p>{(this.props.selRow.WTBPartner === "TRUE") ? <FontAwesomeIcon icon="check" className="active" /> : <FontAwesomeIcon icon="times" className="disabled" /> } WTB Partner </p>
                                            <p>{(this.props.selRow.ConXitPartner === "TRUE") ? <FontAwesomeIcon icon="check" className="active" /> : <FontAwesomeIcon icon="times" className="disabled" /> } ConXit Partner </p>
                                        </Col>
                                    </Row>
                                    <Row className="item-group justify-content-center">
                                        <Col sm={6}>
                                            <p><CountryList selectedCountry={(this.props.selRow == null) ? null : this.props.selRow.LinkedFromHostname} inputID="LinkedFromHostname" selectedOption={false} /></p>
                                            <h6 className="grey-color text-center">Country</h6>
                                        </Col>
                                        <Col sm={6} className="col-left">
                                            <p><CountryList selectedCountry={(this.props.selRow == null) ? null : this.props.selRow.LinkedFromHostname} inputID="LinkedFromHostname" selectedOption={false} /></p>
                                            <h6 className="grey-color text-center">Website</h6>
                                        </Col>                                        
                                    </Row>
                                    <Row className="item-group justify-content-center">
                                        <Col sm={5}>
                                            <p className="text-center">{formatDate(this.props.selRow.CreateOn)}</p>
                                            <h6 className="grey-color text-center">Created</h6>
                                        </Col>  
                                        {(this.props.selRow.last_edit_on != "") ?
                                            <Col sm={5} className="col-left">       
                                                <p className="text-center">{formatDate(this.props.selRow.last_edit_on)}</p>
                                                <h6 className="grey-color text-center">Updated</h6>
                                            </Col>
                                        :
                                            null
                                        }
                                    </Row>
                                </Col>
                                </Col>
                            </Col>

                            <Col sm={8} id="right-column-profile">
                                
                                <Col>
                                    <div className="item-group">
                                        <h6 className="grey-color">Website URL</h6>
                                        <p>{(this.props.selRow.PartnerWebsiteName != "") ? this.props.selRow.PartnerWebsiteName : "-"}</p>
                                    </div>
                                    <div className="item-group">
                                        <h6 className="grey-color">Detail Page (One)</h6> 
                                        <p>{(this.props.selRow.PartnerWebsiteSearchPrefix != "") ? this.props.selRow.PartnerWebsiteSearchPrefix : "-"}</p>
                                    </div>
                                    <div className="item-group">
                                        <h6 className="grey-color">StarTech.com Landing page (All)</h6>  
                                        <p>{(this.props.selRow.PartnerWebsiteAllStartec != "") ? this.props.selRow.PartnerWebsiteAllStartech : "-"}</p>  
                                    </div>            
                                    <div className="item-group">
                                        <h6 className="grey-color">Partner Search (Startech)</h6>  
                                        <p>{(this.props.selRow.PartnerWebsiteRealSearchURL != "") ? this.props.selRow.PartnerWebsiteRealSearchURL : "-"}</p>  
                                    </div>                    
                                    <div className="item-group">
                                        <h6 className="grey-color">ConXit URL</h6>  
                                        <p>{(this.props.selRow.InceptorPrefix != "") ? this.props.selRow.InceptorPrefix : "-"}</p>
                                    </div>

                                    <div className="item-group">
                                        <pre>
                                            <code>
                                                {(this.props.selRow.BorrowSKUsFrom != "") ? 
                                                    `<iframe height='750' src="https://www.startech.com/${this.props.selRow.LinkedFromHostname}/conxit2client?cid=${this.props.selRow.PartnerCode}&link_cid=${this.props.selRow.BorrowSKUsFrom}&usi=TRUE" frameborder='0' width='100%'></iframe>`
                                                : 
                                                    `<iframe height='750' src="https://www.startech.com/${this.props.selRow.LinkedFromHostname}/conxit2client?cid=${this.props.selRow.PartnerCode}" frameborder='0' width='100%'></iframe>`
                                                }
                                            </code>
                                        </pre>
                                        <Button color="primary" onClick={this.iframeToggle} disabled={(this.props.selRow.Active == "TRUE") ? null : "disabled"}>Test</Button>
                                    </div>
                                    {(this.props.selRow.Active == "TRUE") ? 
                                        <Modal id="iframe-partner" isOpen={this.state.iframeModal} toggle={this.iframeToggle}>
                                            <ModalHeader isOpen={this.state.iframeModal}>{this.props.selRow.PartnerName}</ModalHeader>
                                            <ModalBody isOpen={this.state.iframeModal}>
                                                <iframe 
                                                    height="700" 
                                                    src= {
                                                        (this.props.selRow.BorrowSKUsFrom != "") ? 
                                                            `https://www.startech.com/${this.props.selRow.LinkedFromHostname}/conxit2client?cid=${this.props.selRow.PartnerCode}&link_cid=${this.props.selRow.BorrowSKUsFrom}&usi=TRUE`
                                                        : 
                                                            `https://www.startech.com/${this.props.selRow.LinkedFromHostname}/conxit2client?cid=${this.props.selRow.PartnerCode}`
                                                        }
                                                    frameborder="0" 
                                                    width="100%"></iframe>
                                            </ModalBody>
                                            <ModalFooter>
                                                <Button color="secondary" onClick={this.iframeToggle}>Close</Button>
                                            </ModalFooter>
                                        </Modal>

                                    :
                                        null
                                    }

                                    <Row className="item-group">                                
                                        <Col sm={4}>
                                            <h6 className="grey-color">Partner Type</h6>
                                            <p>{(this.props.selRow.PartnerType != "") ? this.props.selRow.PartnerType : "-"}</p>
                                        </Col> 
                                        <Col sm={4}>                
                                            <h6 className="grey-color">Use SKUs from</h6>
                                            <p>{(this.props.selRow.BorrowSKUsFrom != "") ? this.props.selRow.BorrowSKUsFrom : "-"}</p>
                                        </Col>
                                        <Col sm={3}>                
                                            <h6 className="grey-color">Rank</h6>
                                            <p>{(this.props.selRow.Rank != "") ? this.props.selRow.Rank : "-"}</p>
                                        </Col>                                            
                                    </Row>
                                </Col>
                            </Col>
                        </Row>
                    }
                    </div>
                }
            </Col>
        )
    }
}


export default Partner;


const formatDate = (value) => {
    const monthLetter = [
        "Jan", 
        "Feb", 
        "Mar", 
        "Apr", 
        "May", 
        "Jun", 
        "Jul", 
        "Aug", 
        "Sep", 
        "Oct", 
        "Nov", 
        "Dec"
    ];
    var d = new Date(value);

    return monthLetter[d.getMonth()] + " " + d.getDate() + " " + d.getYear();
}

// <FormGroup check inline>
//     <Label check>
//         <Input type="checkbox" value="1" /> Distributor
//     </Label>
// </FormGroup>
// <FormGroup check inline>
//     <Label check>
//         <Input type="checkbox" value="2" /> Etailer
//     </Label>
// </FormGroup>
// <FormGroup check inline>
//     <Label check>
//         <Input type="checkbox" value="3" /> Gov-Ed
//     </Label>
// </FormGroup>
// <FormGroup check inline>
//     <Label check>
//         <Input type="checkbox" value="4" /> Mail
//     </Label>
// </FormGroup>