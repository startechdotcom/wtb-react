import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from 'axios';
import { 
    Col,
    Row,
    Form,
    FormGroup,
    FormText,
    Label,
    Input
} from 'reactstrap'; 
// https://reactstrap.github.io/components/form/

import CountryList from './country-list';

      // Need to generate this list using this json file....
      // http://country.io/data/
      // http://country.io/names.json
const urlCountryNames = "http://country.io/names.json";
var statusCheckbox = [];

class PartnerCreateUpdate extends React.Component { 
    constructor(props){
        super(props);
        this.checkBoxToggle = this.checkBoxToggle.bind(this);


        this.state = {
            countryData: [],
            checkedStatus: false,
            checkedAudit: false,
            statusChanged: false,
            auditChanged: false,
            checkToggle: []
        };
    }    
    componentDidMount() {
        this.setState({ 
            checkedStatus: false,
            checkedAudit: false,
            statusChanged: false,
            auditChanged: false,
            checkToggle: [] 
        });
        this.serverRequest = 
            axios.get(urlCountryNames)
            .then(response => {
                this.setState({ countryData: response.data });
            })
            .catch( error => { console.log(error) } );
    } 
    componentWillUnmount() {
        // this.serverRequest.abort();
    }
    optionPartner = () => {
        let options = [];
            options.push(<option value="" selected>Select...</option>);
        for ( let value of this.props.theValues) {
            if (this.props.selRow == null) {
                options.push(<option value={value.PartnerCode}>{value.PartnerCode}</option>);         
                
            }else {
                if ( value.PartnerCode == this.props.selRow.BorrowSKUsFrom ) {
                    options.push(<option value={value.PartnerCode} selected>{value.PartnerCode}</option>);
                }else {
                    options.push(<option value={value.PartnerCode}>{value.PartnerCode}</option>);
                } 
            }
        }
        return options;
    }
    checkStatus = () => {
        this.setState({ checkedStatus: !this.state.checkedStatus, statusChanged: true });
    }
    checkAudit = () => {
        this.setState({ checkedAudit: !this.state.checkedAudit, auditChanged: true });
    }  

    checkBoxToggle = (id, status) => {
        var result = false;
        statusCheckbox.forEach(function(element) {
            if ( element.key == id ) {
                element.changed = true;
                element.status = !status;
                result = true;
                console.log(status+" aht");
            }
        });
        if ( result == false ) {
            statusCheckbox.push({
                key: id,
                changed: true,
                status: status 
            });
        }
        console.log(statusCheckbox);
        this.setState({ checkToggle: statusCheckbox });
    } 

    render() {
        return (
            <Form className="col">
                <Row>
                    <Col sm={4} id="left-column-profile">
                        <Col className="no-left-padding">
                            <FormGroup>
                                {/* 
                                    https://alligator.io/react/react-dropzone/
                                    https://codesandbox.io/s/547y3p8o9k 
                                */}
                                <Label for="exampleFile">Logo File</Label>
                                <Input type="file" name="file" id="exampleFile" accept="image/gif" />
                                <FormText color="muted">
                                    This is some placeholder block-level help text for the above input.
                                    It's a bit lighter and easily wraps to a new line.
                                </FormText>
                            </FormGroup>
                            <FormGroup>
                                <Label for="partnerName">Company (Partner Name)</Label>
                                <Input type="text" name="partnerName" id="partnerName" placeholder="Company Name" value={(this.props.selRow == null) ? "" : this.props.selRow.PartnerName} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="PartnerCode">Partner Code</Label>
                                <Input type="text" name="PartnerCode" id="PartnerCode" placeholder="Code" className="w-25" value={(this.props.selRow == null) ? "" : this.props.selRow.PartnerCode} />
                            </FormGroup>  
                            <Col>
                            <Row className="item-group bottom">
                                <Col sm={3}>
                                    <FormGroup>
                                        <h6 className="grey-color text-center">Active</h6>
                                        <Label for="Active" check className="text-center toggleSwitch">  
                                            {(this.state.statusChanged) ?
                                                (this.state.checkedStatus) ?
                                                    <Input type="checkbox" name="Active" checked />
                                                :
                                                    <Input type="checkbox" name="Active" />                                            :
                                                (this.props.selRow != null) ?
                                                    (this.props.selRow.Active == "TRUE") ?
                                                        <Input type="checkbox" name="Active" checked />
                                                    :
                                                        <Input type="checkbox" name="Active" />
                                                :
                                                    <Input type="checkbox" name="Active" />
                                            }
                                            <span class="slider round" onClick={this.checkStatus}></span>
                                        </Label>
                                    </FormGroup>   
                                </Col>
                                <Col sm={3} className="col-left">
                                    <FormGroup>
                                        <h6 className="grey-color text-center">Audit</h6>
                                        <Label for="Audit" check className="text-center toggleSwitch">
                                            {(this.state.auditChanged) ?
                                                (this.state.checkedAudit) ?
                                                    <Input type="checkbox" name="Audit" checked />
                                                :
                                                    <Input type="checkbox" name="Audit" />
                                            :
                                                (this.props.selRow != null) ?
                                                    (this.props.selRow.Audit == "TRUE") ?
                                                        <Input type="checkbox" name="Audit" checked />
                                                    :
                                                        <Input type="checkbox" name="Audit" />
                                                :
                                                    <Input type="checkbox" name="Audit" />
                                            }
                                            <span class="slider round" onClick={this.checkAudit}></span>
                                        </Label>
                                    </FormGroup>         
                                </Col>  
                                <Col sm={6} className="col-left no-right-padding">      
                                    <FormGroup>
                                        <Label key="ShowWTBLink" for="ShowWTBLink" check className="toggleCheck"> WTB Link
                                            <Input type="checkbox" name="ShowWTBLink" id="ShowWTBLink" />
                                            {(this.props.selRow == null) ?
                                                <span class="checkmark" onClick={() => { this.checkBoxToggle("ShowWTBLink", false)}}></span>
                                            :
                                                <span class="checkmark" onClick={() => { this.checkBoxToggle("ShowWTBLink", this.props.selRow.ShowWTBLink)}}></span>
                                            }
                                        </Label>
                                        <Label key="WTBPartner" for="WTBPartner" check className="toggleCheck"> WTB Partner  
                                            <Input type="checkbox" name="WTBPartner" id="WTBPartner" />
                                            {(this.props.selRow == null) ?
                                                <span class="checkmark" onClick={() => { this.checkBoxToggle("WTBPartner", false)}}></span>
                                            :
                                                <span class="checkmark" onClick={() => { this.checkBoxToggle("WTBPartner", this.props.selRow.WTBPartner)}}></span>
                                            }                                        
                                        </Label>
                                        <Label key="ConXitPartner" for="ConXitPartner" check className="toggleCheck"> ConXit Partner
                                            <Input type="checkbox" name="ConXitPartner" id="ConXitPartner" />
                                            {(this.props.selRow == null) ?
                                                <span class="checkmark" onClick={() => { this.checkBoxToggle("ConXitPartner", false)}}></span>
                                            :
                                                <span class="checkmark" onClick={() => { this.checkBoxToggle("ConXitPartner", this.props.selRow.ConXitPartner)}}></span>
                                            }                                        
                                        </Label>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Col>
                        </Col>
                    </Col>

                    <Col sm={8} id="right-column-profile">
                        
                        <Col className="no-right-padding">
                            <Row>
                                <Col sm={7}>
                                    <FormGroup>
                                        <Label for="PartnerWebsiteName">Website URL</Label>
                                        <Input type="url" name="PartnerWebsiteName" id="PartnerWebsiteName" placeholder="Website" value={(this.props.selRow == null) ? "" : this.props.selRow.PartnerWebsiteName} />
                                    </FormGroup>
                                </Col>
                                <Col sm={4}>
                                    <CountryList selectedCountry={(this.props.selRow == null) ? null : this.props.selRow.LinkedFromHostname} inputID="LinkedFromHostname" selectedOption={true} label="Country" />
                                </Col>
                            </Row>

                            <FormGroup>
                                <Label for="PartnerWebsiteSearchPrefix">Detail Page (One)</Label> 
                                <Input type="url" name="PartnerWebsiteSearchPrefix" id="PartnerWebsiteSearchPrefix"  placeholder="Detail Page" value={(this.props.selRow == null) ? "" : this.props.selRow.PartnerWebsiteSearchPrefix} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="PartnerWebsiteAllStartech">StarTech.com Landing page (All)</Label>  
                                <Input type="url" name="PartnerWebsiteAllStartech" id="PartnerWebsiteAllStartech" placeholder="Landing page" value={(this.props.selRow == null) ? "" : this.props.selRow.PartnerWebsiteAllStartech} />  
                            </FormGroup>            
                            <FormGroup>
                                <Label for="PartnerWebsiteRealSearchURL">Partner Search (Startech)</Label>  
                                <Input type="url" name="PartnerWebsiteRealSearchURL" id="PartnerWebsiteRealSearchURL" placeholder="Partner Search page" value={(this.props.selRow == null) ? "" : this.props.selRow.PartnerWebsiteRealSearchURL} />   
                            </FormGroup>                    
                            <FormGroup>
                                <Label for="InceptorPrefix">ConXit URL</Label>  
                                <Input type="url" name="InceptorPrefix" id="InceptorPrefix" placeholder="ConXit URL" value={(this.props.selRow == null) ? "" : this.props.selRow.InceptorPrefix} />
                            </FormGroup>
                            <Row>
                                <Col sm={4}>
                                    <CountryList selectedCountry={(this.props.selRow == null) ? null : this.props.selRow.LinkedFromHostname} inputID="LinkedFromHostname" selectedOption={true} label="StarTech.com Website" />
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={4}>
                                    <FormGroup>
                                        <Label for="PartnerType">Partner Type</Label>
                                        <Input type="select" name="PartnerType" id="PartnerType" multiple>
                                        <option value="1">Distributor</option>
                                        <option value="2">ETailer</option>
                                        <option value="3">Gov-Ed</option>
                                        <option value="4">Mail</option>
                                        <option value="5">None</option>
                                        <option value="6">Retail</option>
                                        <option value="7">ENTCOMRES</option>
                                        <option value="8">Regional Distributor</option>
                                        </Input>
                                    </FormGroup>   
                                </Col> 
                                <Col sm={4}>                
                                    <FormGroup>
                                        <Label for="BorrowSKUsFrom">Use SKUs from</Label>
                                        <Input type="select" name="BorrowSKUsFrom" id="BorrowSKUsFrom">
                                            {this.optionPartner()}
                                        </Input>
                                    </FormGroup>
                                </Col> 
                                <Col sm={3}>                
                                    <FormGroup>
                                        <Label for="Rank">Rank</Label>
                                        <Input type="text" name="Rank" id="Rank" placeholder="1000" value={(this.props.Rank == null) ? "" : this.props.selRow.Rank} />
                                    </FormGroup>
                                </Col>                                                               
                            </Row>
                        </Col>
                    </Col>
                </Row>
            </Form>
        )
    }
}


export default PartnerCreateUpdate;