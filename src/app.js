import React from 'react';
import PropTypes from 'prop-types';
import * as Papa from 'papaparse';
import axios from 'axios';

// https://getbootstrap.com/docs/4.0/getting-started/introduction/
// https://reactstrap.github.io/components/alerts/
import {
    Container, 
    Row, 
    Col } from 'reactstrap';

import Header from "./Components/template/header";
import Sidebar from "./Components/template/sidebar";
import WTBtable from './Components/wtb-table';

import './style/flags.scss';
import './style/app.scss';

    const GETurl = 'http://api.qat.dynamics.startechnet.com:80/api/WTB/Partner',
          csvFilePath = "./public/media/wtb-csv.csv";


class App extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            data:[],
            sidebarOpen: false,
            loading: false
        };
        this.updateData = this.updateData.bind(this);
        this.updateIsOpen = this.updateIsOpen.bind(this);

        Container.propTypes = {
            fluid:  PropTypes.bool
        }
    }

    componentDidMount() {
        this.parseCSV();
        this.parseAPI();
        this.updateIsOpen(false);
    } 

    updateIsOpen(value) {
        this.setState({ sidebarOpen: value });
        // console.log('update mood:'+value);

      }
    parseCSV = () => {
        Papa.parse(csvFilePath, {
            download: true,
            header: true,
            complete:  this.updateData
        });
    }
    parseAPI = () => {
        this.serverRequest = 
            axios.get(GETurl)
            .then(response => {
                console.log(response.data);
                //this.setState({ countryData: response.data });
            })
            .catch( error => { console.log(error) } );
    }

    updateData(result) {
        const data = result.data;
        (data !== null) ? this.setState({ loading: true }) : setTimeout(this.updateData(), 1500);
        // for (var property1 in data) {
        //     var code = (typeof data[property1].PartnerCode !== 'undefined') ? 
        //         "https://sgcdn.startech.com/005329/media/pages/wheretobuy/large/"+ data[property1].PartnerCode.toLowerCase() +".large.gif" 
        //         : "none";
        //     data[property1].PartnerCodeLogo = code;
        //     // console.log(data[property1]);
        // }
        
        
        if( this.state.data.length == 0 ) {
            this.setState({data: data}); 
            console.log("DATA del Excel file:  "+data);
        }
    }
    
    render() {
        return ( 
            <div>
                <Header updateIsOpen={this.updateIsOpen} isOpen={this.state.sidebarOpen} />
                <Sidebar isOpen={this.state.sidebarOpen} updateIsOpen={this.updateIsOpen} />
                <section id="main-content">
                {( this.state.loading ) ? 
                    <div className="h-100">
                        {( this.state.data.length > 0 ) ? <WTBtable myData={this.state.data} /> :  null }
                    </div >
                :
                    <Row className="justify-content-between">
                        <Col><img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" alt="" /></Col>
                    </Row>
                }
                </section>
            </div>
        );
    }
}

export default App;